/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Express} app
 * @param {import('supertest')} request
 * @param {number} JEST_TIMEOUT
 */
function main(app, request, JEST_TIMEOUT) {
  describe("/register", () => {
    test("should return 'Register success' with array of data [positive-test]", async () => {
      const response = await request(app)
        .post("/api/register")
        .send({
          username: "tes",
          password: "tes",
        });

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({
        error: false,
        message: "Register success",
        data: [expect.any(Object)]
      });
    }, JEST_TIMEOUT);

    test("should return 'Username already exists' with empty data [negative-test]", async () => {
      const response = await request(app)
        .post("/api/register")
        .send({
          username: "tes",
          password: "tes",
        });

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({
        error: true,
        message: "Username already exists",
        data: []
      });
    }, JEST_TIMEOUT);

    test("should return 'Username and password are required' with empty data [negative-test]", async () => {
      const response = await request(app)
        .post("/api/register")
        .send({
          username: "",
          password: "",
        });

      expect(response.statusCode).toBe(200);
      expect(response.body).toEqual({
        error: true,
        message: "Username and password are required",
        data: []
      });
    });
  });
}

module.exports = main;
