const { PrismaClient } = require('./pgclient')

/** @type {import('./pgclient').PrismaClient} */
const prisma = new PrismaClient()

async function main() {
  //admin user seed
  const admin = await prisma.user_game.upsert({
    where: {
      username: 'admin'
    },
    update: {
      password: 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec',
      is_admin: true
    },
    create: {
      username: 'admin',
      email: 'admin@admin.com',
      password: 'c7ad44cbad762a5da0a452f9e854fdc1e0e7a52a38015f23f3eab1d80b931dd472634dfac71cd34ebc35d16ab7fb8a90c81f975113d6c7538dc69dd8de9077ec',
      is_admin: true
    },
  })

  //admin biodata seed
  await prisma.user_game_biodata.create({
    data: {
      first_name: 'Super',
      last_name: 'admin',
      age: 777,
      about: 'I am super admin',
      user_game: {
        connect: {
          id: admin.id
        }
      }
    }
  })
}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
