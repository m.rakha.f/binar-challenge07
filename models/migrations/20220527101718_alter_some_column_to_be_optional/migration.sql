-- AlterTable
ALTER TABLE "user_game" ALTER COLUMN "email" DROP NOT NULL;

-- AlterTable
ALTER TABLE "user_game_biodata" ALTER COLUMN "last_name" DROP NOT NULL,
ALTER COLUMN "age" DROP NOT NULL,
ALTER COLUMN "about" DROP NOT NULL;
