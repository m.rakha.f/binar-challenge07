/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function middleware(req, res, next) {
  // object has own property
  if(!req.headers || !req.headers.hasOwnProperty('token')) {
    return next();
  }

  const session = await prisma.session.findFirst({
    where: {
      token: req.headers.token,
    },
    include: {
      user_game: true,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(session && !session.error) {
    req.authorized = true;
    req.is_admin = session.user_game.is_admin;
    req.user_id = session.user_game.id;
  }

  next();
}

module.exports = middleware;
