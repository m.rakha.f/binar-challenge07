/** @type {import('../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.authorized) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  const findPayload = req.is_admin ? {} : {where:{user_id:req.user_id}};

  const data = await prisma.user_game_biodata.findMany(findPayload).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error){
    return res.status(500).json(data)
  }

  if(!data || data.length === 0) {
    return res.json({
      error: true,
      message: 'No user biodata found',
      data: [],
    })
  }

  res.json({
    error: false,
    message: 'Success',
    data,
  })
}

module.exports = controller;
