/** @type {import('../../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.authorized) {
    return res.status(401).json({
      error: true,
      message: 'Unauthorized',
      data: [],
    });
  }

  const deleted = await prisma.session.delete({
    where: {
      token: req.headers.token,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(deleted && deleted.error) {
    return res.status(500).json(deleted);
  }

  res.json({
    error: false,
    message: 'Logout success',
    data: [],
  });
}

module.exports = controller;
