const byJwt = require('./jwt');
const byDb = require('./db');
const authMethod = process.env.AUTH_METHOD || 'db';

const availableMethod = {
  db: byDb,
  jwt: byJwt
};

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
function controller(req, res, next){
  //check if authMethod in availableMethod
  if(!availableMethod[authMethod]){
    return availableMethod['db'](req, res, next);
  }

  return availableMethod[authMethod](req, res, next);
}

module.exports = controller;
