const crypto = require('crypto');
/** @type {import('../../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.username || !req.body.password) {
    return res.json({
      error: true,
      message: 'Username and password are required',
      data: [],
    })
  }

  const user = await prisma.user_game.findFirst({
    where: {
      username: req.body.username,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(user && user.error){
    return res.status(500).json(user)
  }

  const hash = crypto.createHash('sha512').update(req.body.password).digest('hex');

  if(
    !user
    || user == null
    || user.password != hash
  ) {
    return res.json({
      error: true,
      message: 'Username or password is incorrect',
      data: [],
    })
  }

  const data = await prisma.session.create({
    data: {
      user_game: {
        connect: {
          id: user.id,
        },
      },
      //randomly generated token
      token: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15),
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error){
    return res.status(500).json(data)
  }

  return res.json({
    error: false,
    message: 'Login success',
    data: [data]
  })
}

module.exports = controller;
